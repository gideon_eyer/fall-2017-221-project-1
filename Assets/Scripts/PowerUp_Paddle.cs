﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp_Paddle : MonoBehaviour {

    public GameObject player1;
    public GameObject player2;
    public PongBallMove pongBallPoint;

	// Use this for initialization
	void Start ()
    {
        player1 = GameObject.FindWithTag("Left");
        player2 = GameObject.FindWithTag("Right");
        pongBallPoint = GameObject.FindWithTag("ball").GetComponent<PongBallMove>();
    }
    
    public void OnTriggerEnter(Collider other)
    {
        if(other.transform.CompareTag("ball"))
        {
            if(pongBallPoint.pointTracker == 0)
            {
                player1.transform.localScale = new Vector3(0.2f, 3f, 1f);
                Destroy(gameObject);
            }
            else
            {
                player2.transform.localScale = new Vector3(0.2f, 3f, 1f);
                Destroy(gameObject);
            }
        }
    }
}
